package com.progressoft.password.generator;

import java.util.Random;

public class PasswordGenerator {
    public static final String SPECIAL_CHAR = "_$#%";
    public static final int A_CAPITAL = 65;
    private final Random random = new Random();

    public String generate() {
        StringBuilder password = new StringBuilder();
        shufflePassword(password);
        return password.toString();
    }

    private void shufflePassword(StringBuilder password) {
        for (int i = 0; i < 2; i++) {
            generateCapitalChar(password);
            generateNumber(password);
            generateSpecialChar(password);
            generateNumber(password);
        }
    }

    private void generateCapitalChar(StringBuilder password) {
        password.append((char) (A_CAPITAL + random.nextInt(26)));
    }

    private void generateNumber(StringBuilder password) {
        password.append(random.nextInt(10));
    }

    private void generateSpecialChar(StringBuilder password) {
        password.append(SPECIAL_CHAR.charAt(random.nextInt(4)));
    }
}
