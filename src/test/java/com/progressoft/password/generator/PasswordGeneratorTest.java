package com.progressoft.password.generator;

import java.util.function.Predicate;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PasswordGeneratorTest {
    public static final int A_ASCII = 65;
    public static final int Z_ASCII = 90;
    public static final int ZERO_ASCII = 48;
    public static final int NINE_ASCII = 57;
    public static final String SPECIAL_CHAR = "_$#%";
    PasswordGenerator passwordGenerator;
    String newPassword;

    @BeforeEach
    void setUp() {
        passwordGenerator = new PasswordGenerator();
    }

    @Test
    void checkPasswordLength() {
        newPassword = passwordGenerator.generate();
        Assertions.assertEquals(8, newPassword.length());
    }

    @Test
    void checkPasswordContainsFourNumbers() {
        newPassword = passwordGenerator.generate();
        int countNum = count(newPassword, this::isNumber);
        Assertions.assertEquals(4, countNum);
    }

    @Test
    void checkPasswordContainsTwoCapitalCharacter() {
        newPassword = passwordGenerator.generate();
        int countChar = count(newPassword, this::isCapitalChar);
        Assertions.assertEquals(2, countChar);
    }

    @Test
    void checkPasswordContainsTwoSpecialChar() {
        newPassword = passwordGenerator.generate();
        int countSpecialChar = count(newPassword, this::isSpecialChar);
        Assertions.assertEquals(2, countSpecialChar);
    }

    @Test
    void multipleCallPasswordGeneratorShouldNotReturnTheSamePassword() {
        String firstPassword = passwordGenerator.generate();
        String secondPassword = passwordGenerator.generate();
        Assertions.assertNotEquals(firstPassword, secondPassword);
    }

    @Test
    void shouldBeShuffledWithoutHavingEachGroupOfCharactersFollowingEachOther() {
        newPassword = passwordGenerator.generate();
        boolean shuffle = true;
        for (int i = 1; i < newPassword.length(); i++) {
            if (isSpecialChar(newPassword.charAt(i - 1)) && isSpecialChar(newPassword.charAt(i))) {
                shuffle = false;
                break;
            } else if (isNumber(newPassword.charAt(i - 1)) && isNumber(newPassword.charAt(i))) {
                shuffle = false;
                break;

            } else if (isCapitalChar(newPassword.charAt(i - 1)) && isCapitalChar(newPassword.charAt(i))) {
                shuffle = false;
                break;
            }
        }
        Assertions.assertTrue(shuffle);
    }

    private boolean isSpecialChar(char charInPassword) {
        return SPECIAL_CHAR.contains(charInPassword + "");
    }

    private boolean isCapitalChar(char charInPassword) {
        return charInPassword >= A_ASCII && charInPassword <= Z_ASCII;
    }

    private boolean isNumber(char charInPassword) {
        return charInPassword >= ZERO_ASCII && charInPassword <= NINE_ASCII;
    }

    private int count(String password, Predicate<Character> condition) {
        int counter = 0;
        for (char charInPassword : password.toCharArray())
            if (condition.test(charInPassword))
                counter += 1;
        return counter;
    }
}